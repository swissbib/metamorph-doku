---
title: "Werte auslesen"
date: 2018-08-10T20:51:00+02:00
anchor: "datenfelder"
weight: 11
---

Werte einzelner Datenfelder können mit der Funktion `data` ausgelesen werden.
In ihrer einfachsten Form hat `data` ein Attribut, `source`, welches dem Namen
des auszulesenden Feldes entspricht. Dabei können auch Wildcards verwendet
werden, so `?` für ein beliebiges Zeichen und `*` für eine Sequenz beliebiger
Zeichen mit Länge 0-n. In dieser einfachsten Form - ausgeschrieben also bspw.
`<data source="100*.a"/>` - wird lediglich definiert, dass Werte in den
Feldern, welche auf den Ausdruck `100*.a` passen, ebenso wie die Feldnamen
unverändert übernommen werden sollen.

{{% notice warning %}}
Beim Wildcard-Charakter `*` ist Vorsicht zu walten.

Ein Beispiel: Mit dem Muster `085*` können in einer MARC-XML-Struktur alle
`085`-Felder mit beliebigen Indikatoren gematcht werden, gleichzeitig aber auch alle ihre Unterfelder (bspw. `085  .a`). Dies kann insbesondere zu Problemen bei der Verwendung im [`flushWith`]({{< ref "/collectors/ausgabesteuerung.md" >}})-Parameter in [collectors]({{< ref "/collectors/uebersicht.md" >}}) führen. So wird bei einer gegebenen Struktur 

```xml
<datafield tag="085" ind1=" " ind2=" ">
  <subfield code="8">1.1</subfield>
  <subfield code="b">346.046</subfield>
  <subfield code="a">346.046</subfield>
  <subfield code="r">333</subfield>
  <subfield code="s">95</subfield>
</datafield>
<datafield tag="085" ind1=" " ind2=" ">
  <subfield code="8">1.1</subfield>
  <subfield code="b">599</subfield>
  <subfield code="z">1</subfield>
  <subfield code="s">09</subfield>
</datafield>
```

und einer `combine`-Anweisung

```xml
<combine name="085" value="${b}" reset="true" flushWith="085*">
  <data source="085??.b" name="b"/>
</combine>
```

folgendes ausgegeben:

```
{085: , 085: 346.046, 085: , 085: , 085: , 085: }
{085: , 085: 599, 085: , 085: , 085: }
```

Das Problem liegt darin, dass nach jedem Ereignis in Feld `085` ein Flush
ausgeführt und der möglicherweise gesammelte Wert zurückgesetzt wird (für eine Erklärung zu den Mechanismen von `flushWith` siehe
Kapitel [Ausgabesteuerung]({{< ref "/collectors/ausgabesteuerung.md" >}})).
D.h., ein Flush erfolgt also nach dem Ende jedes Unterfeldes __und zusätzlich__ nach dem Ende des Feldes
selbst. Viel besser wäre in diesem Fall ein gezieltes Flush, das __nur__ nach
dem Ende des gewünschten Unterfeldes durchgeführt wird, was mit `flushWith="085??.b"` bewerkstelligt werden kann.
{{% /notice %}}

Zudem gibt es für `source` zwei Variablen mit fixer Bedeutung:

- `_id`: Liest die ID des Datensatzes aus.
- `_else`: Prozessiert alle Literale, welche nicht durch eine andere
  Morph-Regel verarbeitet werden. Nützlich, wenn Literale nicht
transformiert, aber auch nicht ausgefiltert werden sollen.

`data` nimmt aber noch ein zweites Attribut entgegen, `name`. Dadurch lassen
sich Feldnamen umbenennen, während der Feldwert noch immer unverändert
übernommen wird. Durch den Feldnamen `_id` wird die ID des Datensatzes
überschrieben. 

{{% notice note %}}
Datenfelder mit Namen, die mit `@` beginnen, werden [speziell]({{< ref "/modularisierung/rekursion.md" >}}) behandelt.
{{% /notice %}}

Schliesslich lassen sich auf Feldwerte eine Reihe von Transformationen und
Filter anwenden.
